package thebestteam.beansgenerator;

import java.io.PrintWriter;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Build all beans from database informations
 */

public class Factory
{
	
	static Connection conn = null;
	static Statement stmt = null;
	private String path;
	private String schema;
	private String type;
	private String username;
	private String password;
	private int port;
	private String package_;
	private String inherited;
	private boolean removeUnderscore = true;
	private boolean singular = false;
	
	public String getPath()
	{
		return path;
	}

	public boolean isRemoveUnderscore() {
		return removeUnderscore;
	}

	public boolean isSingular() {
		return singular;
	}

	public void setSingular(boolean singular) {
		this.singular = singular;
	}

	public void setRemoveUnderscore(boolean removeUnderscore) {
		this.removeUnderscore = removeUnderscore;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public String getSchema()
	{
		return schema;
	}

	public void setSchema(String schema)
	{
		this.schema = schema;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public int getPort()
	{
		return port;
	}

	public void setPort(int port)
	{
		this.port = port;
	}

	public String getPackage_()
	{
		return package_;
	}

	public void setPackage_(String package_)
	{
		this.package_ = package_;
	}

	public String getInherited()
	{
		return inherited;
	}

	public void setInherited(String inherited)
	{
		this.inherited = inherited;
	}

	private final static String HEADER = "/**\n" + 
			" * Bean built automatically\n" + 
			" * Date: "+LocalDate.now()+"\n" + 
			" * Time: "+LocalTime.now()+"\n" + 
			" * By BEANS GENERATOR 1.2\n" + 
			" * Author: Descartes OUAMBO FOWO -- descartesouambo@gmail.com\n" + 
			" * Author: Carrel MEZATSONG TSAFACK -- meztsacar@gmail.com\n" + 
			" */"; 
	private static PrintWriter writer;
	

	/**
     * Explore our database
     * @throws multiple Exceptions
     */
	public void explorer() throws Exception
	{
		//Establishing connection with our DB

		connection();
		DatabaseMetaData md = conn.getMetaData();
	    stmt = conn.createStatement();
	    String[] types = { "TABLE" };
	    ResultSet rst = md.getTables(null, null, "%", types);
	    System.out.println("Destination path : " + getPath());
	    while (rst.next()) 
    	{ 
		  String tableName = rst.getString(3);
		  String tableNameWS = singular(tableName);
		  
		  ResultSet rsc = stmt.executeQuery("SELECT * FROM "+tableName);
		  ResultSetMetaData rsmd = rsc.getMetaData();	  
		  int columnCount = rsmd.getColumnCount();
		  String fileName = removeUnderscore?isUnderscored(capitalize(tableNameWS)+".java"):capitalize(tableNameWS)+".java";
		  String filePath = Paths.get(getPath()).resolve(fileName).toFile().getAbsolutePath();
		  System.out.print("Making "+filePath+" ...");
		  writer = new PrintWriter(filePath, "UTF-8");
		  
		  if(! getPackage_().isEmpty())
		  {
			  writer.println("package "+getPackage_()+";\n");
		  }
		  
		  String className = removeUnderscore?isUnderscored(capitalize(tableNameWS)):capitalize(tableNameWS);

		  if(getInherited().isEmpty())
		  {
			  writer.println(HEADER+"\n");
			  writer.println("public class "+className+" \n{\n");
		  }
		  else
		  {
			  writer.println("import "+getInherited()+";\n");
			  writer.println(HEADER+"\n");
			  writer.println("public class "+className+" extends "+getInherited().substring(getInherited().lastIndexOf(".")+1, getInherited().length())+" \n{\n");

		  }

		  // The column count starts from 1

		  for (int i = 1; i <= columnCount; i++ ) 
		  {
		    String name = rsmd.getColumnName(i);
		    String nameColumn = removeUnderscore?isUnderscored(name):rsmd.getColumnName(i);
		    String type = getJavaType(getSchema(), tableName, name);
		    writer.println("\tprivate "+type+" "+nameColumn+";");
		  }
		  
		  writer.println("\n");
		  
		  for (int i = 1; i <= columnCount; i++ ) 
		  {
		    String name = rsmd.getColumnName(i);
		    String type = getJavaType(getSchema(), tableName, name);
		    writer.println(getter(name, type));
		    writer.println(setter(name, type));
		  }
		  
		  writer.println("} \n");
		  writer.close();
		  
		  System.out.println("Done !"); 
	    }
	    rst.close();
	    conn.close();
	}
		
	/**
     * Establish a connection between app and database
     * @return Connection object
     */
	public Connection connection() throws Exception
	{
		if( conn == null || conn.isClosed())
		{
			if(getType().toLowerCase().equals("mysql"))
			{ 
				conn = DriverManager.getConnection("jdbc:mysql://localhost:"+getPort()+"/"+getSchema()+"?" + "user="+getUsername()+"&password="+getPassword()+"");
			}
			else if(getType().toLowerCase().equals("postgresql"))
			{
				conn = DriverManager.getConnection("jdbc:postgresql://localhost:"+getPort()+"/"+getSchema(), getUsername(), getPassword());
			}
			else
			{
				conn = DriverManager.getConnection("jdbc:sqlite:"+getSchema());
			}
		} 
		return conn;
	}
	
	/**
     * Capitalize a string
     * @param line is string to capitalize
     * @return String capilatized string
     */
	private String capitalize(final String line) 
	{
	   return Character.toUpperCase(line.charAt(0)) + line.substring(1);
	}
	
	/**
     * Build getter from column
     * @param name of column
     * @param type of column
     * @return String that is our getter
     */
	private String getter(String name, String type)
	{
	    name = removeUnderscore?isUnderscored(name):name;
		return "\tpublic "+type+" get"+capitalize(name)+"()\n\t{\n\t\treturn "+name+";\n\t}\n";
	}
	
	private String singular(String text)
	{
		if(isSingular())
		{
			StringBuilder sb = new StringBuilder(text);
			if(sb.charAt(sb.length()-1) == 's')
			{
				return sb.deleteCharAt(text.length()-1).toString();
			}
			else
			{
				return sb.toString();
			}
		}
		else
		{
			return text;
		}
	}
	
	/**
     * Build setter from column
     * @param name of column
     * @param type of column
     * @return String that is our setter
     */
	private String setter(String name, String type)
	{
		name = removeUnderscore?isUnderscored(name):name;
		return "\tpublic void set"+capitalize(name)+"("+type+" "+name+")\n\t{\n\t\tthis."+name+" = "+name+";\n\t}\n";
	}
	   
    
    /**
     * Retrieve a java type of column in string
     * @param schema Database name
     * @param table Table name
     * @param column Column name
     * @return String to be written in raw file as field type
     * @throws Exception if database column is not found
     */
    private String getJavaType( String schema, String table, String column ) throws Exception {
	    
		String fullName = schema + '.' + table + '.' + column;
	    DatabaseMetaData metaData = connection().getMetaData();
	    ResultSet columnMeta = metaData.getColumns( null, null, table, column );
	    
	    String javaType = "Object";
	
	    if( columnMeta.next() ) {
	      int dataType = columnMeta.getInt( "DATA_TYPE" );
	      int columnSize = columnMeta.getInt( "COLUMN_SIZE" );
	      javaType = toClass( dataType , columnSize ).getSimpleName();
	    }
	    else {
	      throw new Exception( "Unknown database column " + fullName + '.' );
	      
	    }
	
	    return javaType;
	}
    
    /**
     * Remove all underscore occurrences
     * @param text to convert
     * @return String without underscore
     */
    private String isUnderscored(String text)
    {
    	int i;
		StringBuilder sb = new StringBuilder(text);
		int length = text.length();
    	for(i=0; i<length-1; i++)
    	{
    		if(text.charAt(i) == '_')
    		{
    			char c = sb.charAt(i+1);
    			sb.replace(i, i+1, String.valueOf(c).toUpperCase());
    			sb.deleteCharAt(i+1);
    			length--;
    		}
    	}
    	if(text.charAt(text.length()-1) == '_')
    		sb.deleteCharAt(text.length()-1);
    	
    	return sb.toString();
    }
        
    
    /**
     * Translates a data type from an integer (java.sql.Types value) to a string
     * that represents the corresponding class.
     * 
     * @param type
     *            The java.sql.Types value to convert to its corresponding class.
     * @return The class that corresponds to the given java.sql.Types
     *         value, or Object.class if the type has no known mapping.
     */
    private static Class<?> toClass(int type, int size) { 
    	
    	Class<?> result = Object.class;

        switch (type) {
            case Types.CHAR:
            	result = Character.class;
            	
            case Types.VARCHAR:
            case Types.LONGVARCHAR:
                result = String.class;
                break;

            case Types.BIT:
                result = Boolean.class;
                break;

            case Types.TINYINT:
                result = ( size == 1 ? Boolean.class : Integer.class );
                break;

            case Types.SMALLINT:
            case Types.INTEGER:
                result = (size <= Integer.MAX_VALUE ? Integer.class : Long.class );
                break;

            case Types.BIGINT:
                result = Long.class;
                break;
                
            case Types.FLOAT:
                result = Float.class;
                break;
                

            case Types.REAL:
            case Types.NUMERIC:
            case Types.DECIMAL:
            case Types.DOUBLE:
                result = Double.class;
                break;

            case Types.BINARY:
            case Types.VARBINARY:
            case Types.LONGVARBINARY:
            case Types.BLOB:
                result = Byte[].class;
                break;

            case Types.DATE:
                result = java.sql.Date.class;
                break;
                
            case Types.TIME_WITH_TIMEZONE:
            case Types.TIME:
                result = java.sql.Time.class;
                break;

            case Types.TIMESTAMP_WITH_TIMEZONE:
            case Types.TIMESTAMP:
                result = java.sql.Timestamp.class;
                break;
        }

        return result;
    }
}