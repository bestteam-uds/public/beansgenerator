package thebestteam.beansgenerator;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;

public class Controller implements Initializable {
	
	@FXML private TextField pathField;
	@FXML private Button pathBut;
	@FXML private Button generateBut;
	@FXML private ComboBox<String> sgbd;
	@FXML private TextField schema;
	@FXML private TextField username;
	@FXML private PasswordField password;
	@FXML private TextField port;
	@FXML private CheckBox removeUnderscore;
	@FXML private CheckBox singular;
	@FXML private TextField package_;
	@FXML private TextField inherited;
	@FXML private CheckBox exitOnFinish;
	private boolean isSQLite = false;
		
	private static Map<String,Integer> sgbdPort;
	
	public void choosePath()
	{
		File initialDir = new File(pathField.getText());
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("Choose destination path in which files will be created");
		if(!initialDir.exists() || !initialDir.isDirectory()){
			initialDir = null;
		}
		chooser.setInitialDirectory(initialDir);
		File dir = chooser.showDialog(null);
		if(dir != null && dir.isDirectory()){
			if(dir.canWrite())
				pathField.setText(dir.getAbsolutePath());
			else
				MessageBox.error("Error", "Permission denied to write into "+dir.getAbsolutePath());
		}
	}
	
	
	public void changePort()
	{
		int iPort = sgbdPort.get(sgbd.getSelectionModel().getSelectedItem());
		port.setText(String.valueOf(iPort));
		isSQLite = (iPort == 0);
		if(isSQLite){
			username.setDisable(true);
			password.setDisable(true);
			port.setDisable(true);
			schema.setPromptText("Fill by absolute database path");
		}else{
			username.setDisable(false);
			password.setDisable(false);
			port.setDisable(false);
			schema.setPromptText("required");
		}
	}
	
	
	public void generate()
	{
		Factory factory = new Factory();
		
		String path = pathField.getText();
		if(path.isEmpty() || !Files.exists(Paths.get(path))){
			MessageBox.error("Path error", "Please choose correct path");
			return;
		}
		factory.setPath(path);
		
		
		String userName = username.getText(), dbName = schema.getText();
		
		if(dbName.isEmpty()){
			MessageBox.error("Database name error", "database name is required");
			return;
		}
		factory.setSchema(dbName);
		
		if(!isSQLite)
		{
			if(userName.isEmpty()){
				MessageBox.error("Username error", "Username is required");
				return;
			}
		}
		factory.setUsername(userName);
		
		int iPort = 0;
		try{
			iPort = Integer.parseInt(port.getText());
			if(iPort < 0 || iPort > 65536){
				MessageBox.error("Port error", "Invalid port range");
				return;
			}
		}catch(NumberFormatException e){
			MessageBox.error("Port error", "Port must be integer between 0 and 65536");
			return;
		}
		factory.setPort(iPort);
		
		String pckg = package_.getText();
		if(!pckg.toLowerCase().equals(pckg)){
			MessageBox.error("Package", "Package must be in lower case");
			return;
		}
		
		if(pckg.contains(" ") || pckg.contains("-") || pckg.endsWith(".")){
			MessageBox.error("Package", "Invalid package name : "+pckg);
			return;
		}
		factory.setPackage_(pckg);
		
		String inheritedClass = inherited.getText();
		if(!inheritedClass.isEmpty() && (!inheritedClass.contains(".") || inheritedClass.endsWith("."))){
			MessageBox.error("Inherited class error", "Please enter full name of class (with package)");
			return;
		}
		
		factory.setInherited(inheritedClass);
		factory.setPassword(password.getText());
		factory.setType(sgbd.getSelectionModel().getSelectedItem());
		factory.setRemoveUnderscore(removeUnderscore.isSelected());
		factory.setSingular(singular.isSelected());
		//now all is ok
		
		try {
			factory.connection();
		} catch (Exception e) {
			MessageBox.error("Connection error", e.getMessage());
			return;
		}
	
		Alert alert = new Alert(
                Alert.AlertType.INFORMATION,
                "Operation in progress",
                ButtonType.CANCEL
        );
        alert.setTitle("Running Operation");
        alert.setHeaderText("Please wait... ");
        ProgressIndicator progressIndicator = new ProgressIndicator();
        alert.setGraphic(progressIndicator);
        alert.getButtonTypes().clear();
        
		Service<Void> service = new Service<Void>(){
	       @Override
	       protected Task<Void> createTask() {
		       return new Task<Void>() {
		          @Override
		          protected Void call() throws Exception {
		        	  factory.explorer();
		        	  return null;
		          };
		      };
	       }
	    };
	    
	    
	    service.setOnSucceeded(e -> {
	    	alert.getButtonTypes().add(ButtonType.CLOSE);
	    	alert.close();
	    	MessageBox.information("Operation completed", "Enjoy your beans :-) ");
	    	if(exitOnFinish.isSelected()){
	    		Platform.exit();
	    	}
	    });
	    service.setOnFailed(e -> {
	    	alert.getButtonTypes().add(ButtonType.CLOSE);
	    	alert.close();
	    	e.getSource().getException().printStackTrace();
	    	MessageBox.error("Processing error", e.getSource().getException().getMessage());
	    	if(exitOnFinish.isSelected()){
	    		Platform.exit();
	    	}
	    });           
	   
	    service.start();
	    alert.show();
	  
	}


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		sgbdPort = new HashMap<>();
		sgbdPort.put("MySQL", 3306);
		sgbdPort.put("PostgreSQL", 5432);
		sgbdPort.put("SQLite", 0);
		sgbd.getItems().addAll(sgbdPort.keySet());
		sgbd.getSelectionModel().select(1);
		int iPort = sgbdPort.get(sgbd.getSelectionModel().getSelectedItem());
		port.setText(String.valueOf(iPort));
		removeUnderscore.setSelected(true);
		singular.setSelected(false);
		
	}
}
